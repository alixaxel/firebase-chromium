# firebase-chromium

Chromium Binary for Firebase/Google Cloud Functions

## Install

```shell
$ gitlab:alixaxel/firebase-chromium#v67.0.3372
```

This will ship with appropriate binary for the latest stable release of [`puppeteer`](https://github.com/GoogleChrome/puppeteer) (usually updated within a day).

## API

| Method             | Returns              | Description                                               |
| ------------------ | -------------------- | --------------------------------------------------------- |
| `args()`           | `{!Array<string>}`   | Provides a list of recommended additional Chromium flags. |
| `executablePath()` | `{?string}`          | Returns the path where the Chromium binary was extracted. |

## Usage

```javascript
const chromium = require('firebase-chromium');
const puppeteer = require('puppeteer');
const functions = require('firebase-functions');

exports.title = functions.https.onRequest(
  async (request, response) => {
    let result = null;
    let browser = null;

    try {
      browser = await puppeteer.launch({
        args: chromium.args(),
        executablePath: chromium.executablePath(),
      });

      let url = 'https://example.com';
      let page = await browser.newPage();

      if (request.body.hasOwnProperty('url') === true) {
        url = request.body.url;
      }

      await page.goto(url);

      result = await page.title();
    } catch (error) {
      return response.sendStatus(500);
    } finally {
      if (browser !== null) {
        await browser.close();
      }
    }

    return response.json(result);
  }
);
```

To prevent `puppeteer` from downloading Chromium, create a `.npmrc` file with the content:

```
puppeteer_skip_chromium_download = "true"
```

Also, Chromium needs a Cloud Function with at least 512MB of RAM.

## Versioning

This package is versioned based on the underlying Chromium version.

## License

MIT
