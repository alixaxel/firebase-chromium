const fs = require('fs');
const path = require('path');

class Chromium {
  /**
   * Returns a list of recommended additional Chromium flags.
   */
  public static args(): string[] {
    let result = [
      '--disable-dev-shm-usage',
      '--disable-notifications',
      '--disable-offer-store-unmasked-wallet-cards',
      '--disable-offer-upload-credit-cards',
      '--disable-setuid-sandbox',
      '--enable-async-dns',
      '--enable-simple-cache-backend',
      '--enable-tcp-fast-open',
      '--media-cache-size=33554432',
      '--no-default-browser-check',
      '--no-first-run',
      '--no-pings',
      '--no-sandbox',
      '--no-zygote',
      '--prerender-from-omnibox=disabled',
    ];

    if (process.env.X_GOOGLE_FUNCTION_NAME !== undefined) {
      result.push('--single-process');
    } else {
      result.push('--start-maximized');
    }

    return result;
  }

  /**
   * Patches the `LD_LIBRARY_PATH` environment variable to look for NSS.
   * Returns the path to the current version of the Chromium binary.
   */
  public static executablePath(): string {
    if (process.env.X_GOOGLE_FUNCTION_NAME === undefined) {
      return null;
    }

    let nss = path.join(__dirname, '..', 'bin', 'nss', 'lib');
    let input = path.join(__dirname, '..', 'bin', 'chromium');
    let output = path.join('/tmp', 'chromium');

    if (process.env.LD_LIBRARY_PATH === undefined) {
      process.env.LD_LIBRARY_PATH = nss;
    } else if (process.env.LD_LIBRARY_PATH.includes(nss) !== true) {
      process.env.LD_LIBRARY_PATH = [nss, process.env.LD_LIBRARY_PATH].join(':');
    }

    if (fs.existsSync(output) !== true) {
      fs.symlinkSync(input, output);
    }

    return output;
  }
}

module.exports = Chromium;
